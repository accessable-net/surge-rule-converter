# Surge Rule Converter

Fetch Surge configuration from a source and convert it into a format that can be easily consumed by [Surgio-libre](https://gitlab.com/accessable-net/surgio-libre).

## Usage

````bash
> python3 ./rule_fetcher.py https://example.com/rules.conf
````