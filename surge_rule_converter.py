#! /usr/bin/python3

import re
from urllib import parse, request
import argparse

from validators import url as validate_url

RULE_CONF = (
    "https://raw.githubusercontent.com/lhie1/Rules/master/Surge/Surge%203/Rule.conf"
)
REJECT_LIST = ["AdBlock"]
DIRECT_LIST = ["Netease Music", "Domestic", "AsianTV"]


class Rule(object):
    # Example:
    # RULE-SET,https://raw.githubusercontent.com/lhie1/Rules/master/Surge/Surge%203/Provider/Reject.list,AdBlock
    def __init__(self, surge_rule_set: str):
        super().__init__()

        surge_rule_set = surge_rule_set.split(",")
        surge_rule_set_url = surge_rule_set[1]
        self.url = surge_rule_set_url

        self.name: str = surge_rule_set_url.split("/").pop().split(".")[0]
        self.name = parse.unquote(self.name)
        self.name = "".join(re.findall("\w+", self.name))

        if surge_rule_set[-1] in REJECT_LIST:
            self.exit = "Reject"
        elif surge_rule_set[-1] in DIRECT_LIST:
            self.exit = "Direct"
        else:
            self.exit = "Proxy"

    def get_surgio_conf(self):
        return {"name": self.name, "url": self.url}

    def has_url(self):
        return validate_url(self.url)

    def get_clash_tpl(self):
        if self.exit == "Reject":
            return "{{ remoteSnippets." + self.name + ".main('REJECT') | clash }}"
        elif self.exit == "Direct":
            return "{{ remoteSnippets." + self.name + ".main('DIRECT') | clash }}"
        return "{{ remoteSnippets." + self.name + ".main('🚀 Proxy') | clash }}"


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Fetch Surge configuration from a source and convert it into a format that can be easily consumed by Surgio."
    )
    parser.add_argument(
        "source_url",
        metavar="N",
        type=str,
        nargs="+",
        help="Url to the source which you want to convert from",
        default=RULE_CONF
    )
    args = parser.parse_args()

    with request.urlopen(args.source_url) as response:
        conf = response.readlines()

    conf = map(lambda byte: byte.decode(), conf)

    # Find out lines containing rules
    rulesets = filter(lambda l: l.startswith("RULE-SET"), conf)

    # Serilize rules
    rulesets = map(lambda l: l.strip(), rulesets)
    rulesets = map(Rule, rulesets)

    # Cross out built-in rules
    rulesets = list(filter(lambda ruleset: ruleset.has_url(), rulesets))

    for r in rulesets:
        print(r.get_surgio_conf(), ",", sep="")

    for r in rulesets:
        print(r.get_clash_tpl(), ",", sep="")
